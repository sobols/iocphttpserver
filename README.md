# IOCP HTTP Server #

This is a sample homework project for "Design and Development of Highload Web-Systems" course.

### Technology ###

The repository contains a single Solution with no dependencies that can be built using Visual Studio 2013 for Windows Desktop. The code uses C++11 features, so at least version 2013 is required.

The server uses Input/output completion port (IOCP) API provided by Microsoft Windows operating system. This API is designed for building high-performance network I/O-intensive applications.

In this simple implementation there are two running threads. Main thread holds a socket that is listened and runs a loop that accepts incoming connections. Non-blocking accept approach through WSA events is used. The thread also prints some statistics on the number of open connections. There is an ability of graceful shutdown, though it is not fully implemented yet. When the connection is accepted, an overlapped read operation is initialized (in order to receive HTTP request). The completion of this operation is managed by the second (worker) thread. The thread runs a loop on I/O completion port handle: it waits for new competed I/O ops and starts the new I/O ops if needed. HTTP request processing is organized as a finite-state machine. Though HTTP support is very limited, it can be easily improved.

If you are interested in IOCP, you may look at the following articles:

* [MSDN](http://msdn.microsoft.com/en-us/library/aa365198(VS.85).aspx)
* [A simple application using I/O Completion Ports and WinSock](http://www.codeproject.com/Articles/13382/A-simple-application-using-I-O-Completion-Ports-an)
* [A simple IOCP Server/Client Class](http://www.codeproject.com/Articles/10330/A-simple-IOCP-Server-Client-Class)
* [IOCP Introduction](http://www.coastrd.com/iocp-introduction)
* [IOCP HTTP Server Design](http://www.coastrd.com/windows-iocp)

### How to use? ###

* http://localhost/ — "Hello, world!" text;
* http://localhost/N — numbers 0, 1, 2, ..., N-1 separated with spaces.

### Benchmark ###

The task was to make a web server that can serve 10.000 HTTP connections simultaneously. Running ApacheBench (ab) benchmark shows that the server meets the requirement.
