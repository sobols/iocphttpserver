#pragma once

#include "data_region.h"

#include <memory>
#include <string>

class TSequentialNumbersData;

class TContentToServe {
public:
    TContentToServe();
    ~TContentToServe();

    TDataRegion GetResource(const std::string& url) const;

private:
    std::unique_ptr<TSequentialNumbersData> Impl;
};
