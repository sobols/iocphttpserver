#include "content.h"

#include <vector>
#include <sstream>

const size_t SERVING_NUMBER_COUNT = 1000000;
std::string HELLO_WORLD = "Hello, world!";

/// Dummy data to serve
class TSequentialNumbersData {
public:
    TSequentialNumbersData(size_t maxNumber)
        : BlockLength(maxNumber + 1)
    {
        std::ostringstream oss;
        BlockLength[0] = 0;
        for (size_t i = 0; i < maxNumber; ++i) {
            if (i > 0) {
                oss << ' ';
            }
            oss << i;
            BlockLength[i + 1] = static_cast<size_t>(oss.tellp());
        }
        Data = std::move(oss.str());
    }

    const std::string& GetData() const {
        return Data;
    }
    size_t GetMaxN() const {
        return BlockLength.size() - 1;
    }
    size_t GetSubstringLength(size_t n) const {
        return BlockLength.at(n);
    }

private:
    std::string Data;
    std::vector<size_t> BlockLength;
};

TContentToServe::TContentToServe()
    : Impl(new TSequentialNumbersData(SERVING_NUMBER_COUNT))
{
}

TContentToServe::~TContentToServe()
{
}

TDataRegion TContentToServe::GetResource(const std::string& url) const
{
    if (url.empty() || url[0] != '/') {
        return TDataRegion();
    }
    const std::string text = url.substr(1);

    if (text.empty()) {
        return TDataRegion(HELLO_WORLD.c_str(), HELLO_WORLD.size());
    }

    size_t pos = 0;
    unsigned long n = 0;

    try {
        n = std::stoul(text, &pos);
    } catch (...) {
        return TDataRegion();
    }

    if (pos == text.length()) { // fully converted
        if (n <= Impl->GetMaxN()) {
            return TDataRegion(Impl->GetData().c_str(), Impl->GetSubstringLength(n));
        }
    }
    return TDataRegion();
}
