#pragma once

#include "data_region.h"

#include <cassert>
#include <cstring>
#include <string>

class TMemoryInput {
public:
    TMemoryInput()
        : Pos(0)
        , End(0)
    {
    }
    explicit TMemoryInput(const TDataRegion& region)
        : Pos(region.GetData())
        , End(region.GetData() + region.GetLength())
    {
    }
    explicit TMemoryInput(const char* data)
        : TMemoryInput(TDataRegion(data, strlen(data)))
    {
    }

    bool HasMoreData() const {
        return Pos != End;
    }
    char ReadChar() {
        assert(HasMoreData());
        const char ch = *Pos;
        ++Pos;
        return ch;
    }

private:
    const char* Pos;
    const char* End;
};


class THttpRequest {
public:
    enum class EStatus {
        Incomplete,
        OK,
        Fail
    };
    enum EMethod {
        Unknown,
        Get,
        Head,
    };

private:
    // parsing
    EStatus Status;
    size_t LineNumber;
    std::string Line;
    bool LastCharWasCR;

    // already parsed
    EMethod Method;
    std::string URI;

public:
    THttpRequest()
        : Status(EStatus::Incomplete)
        , LineNumber(0)
        , LastCharWasCR(false)
        , Method(EMethod::Unknown)
    {
    }

    EStatus GetStatus() const {
        return Status;
    }

    bool IsComplete() const {
        return Status != EStatus::Incomplete;
    }

    EMethod GetMethod() const {
        return Method;
    }
    const std::string& GetURI() const {
        return URI;
    }
    void Read(TMemoryInput* in) {
        while (Status == EStatus::Incomplete && in->HasMoreData()) {
            Consume(in->ReadChar());
        }
    }
private:
    void Consume(char ch) {
        assert(Status == EStatus::Incomplete);

        if (!LastCharWasCR) {
            if (ch == '\r') {
                LastCharWasCR = true;
            } else {
                Line += ch;
            }

        } else {
            if (ch == '\n') {
                LastCharWasCR = false;
                ProcessLine();
                Line.clear();

            } else if (ch == '\r') {
                LastCharWasCR = true;
                Line += '\r'; // for prev. CR

            } else {
                LastCharWasCR = false;
                Line += '\r'; // for prev. CR
                Line += ch;
            }
        }
    }

    void ProcessLine() {
        if (Line.empty()) {
            if (LineNumber > 0) {
                Status = EStatus::OK;
            } else {
                Status = EStatus::Fail;
            }
        } else {
            if (LineNumber == 0) {
                ParseFirstRequestLine();
            } else {
                // Ignore headers
            }
        }
        ++LineNumber;
    }

    void ParseFirstRequestLine() {
        size_t pos = Line.find(' ');
        if (pos == std::string::npos) {
            Status = EStatus::Fail;
            return;
        }
        const std::string methodStr = Line.substr(0, pos);
        if (methodStr == "GET") {
            Method = EMethod::Get;
        } else if (methodStr == "HEAD") {
            Method = EMethod::Head;
        } else {
            Status = EStatus::Fail;
            return;
        }

        URI = Line.substr(pos + 1);
        pos = URI.find(' ');
        if (pos != std::string::npos) {
            // cut HTTP/1.1
            URI.resize(pos);
        }
    }
};

void UnitTestHttpRequest()
{
    {
        THttpRequest request;
        TMemoryInput inp;

        inp = TMemoryInput("GET /example/page HTTP/1.");
        request.Read(&inp);
        assert(request.GetStatus() == THttpRequest::EStatus::Incomplete);

        inp = TMemoryInput("1\r");
        request.Read(&inp);
        assert(request.GetStatus() == THttpRequest::EStatus::Incomplete);

        inp = TMemoryInput("\nHeader: blahblah");
        request.Read(&inp);
        assert(request.GetStatus() == THttpRequest::EStatus::Incomplete);

        inp = TMemoryInput("blah\r\nHost: example.com\r\n");
        request.Read(&inp);
        assert(request.GetStatus() == THttpRequest::EStatus::Incomplete);

        inp = TMemoryInput("\r\nHEAD /\r\n\r\n");
        request.Read(&inp);
        assert(request.GetStatus() == THttpRequest::EStatus::OK);
        assert(request.GetMethod() == THttpRequest::EMethod::Get);
        assert(request.GetURI() == "/example/page");
        assert(inp.HasMoreData());

        // the stream is not finished

        THttpRequest nextRequest;
        nextRequest.Read(&inp);
        assert(nextRequest.GetStatus() == THttpRequest::EStatus::OK);
        assert(nextRequest.GetMethod() == THttpRequest::EMethod::Head);
        assert(nextRequest.GetURI() == "/");
        assert(!inp.HasMoreData());
    }
    {
        THttpRequest request;
        TMemoryInput inp;

        inp = TMemoryInput("POST /example/page HTTP/1.1\r\n\r\n");
        request.Read(&inp);
        assert(request.GetStatus() == THttpRequest::EStatus::Fail);
    }
}
