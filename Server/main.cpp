#include "stdafx.h"

#include "content.h"
#include "data_region.h"
#include "memory_pool.h"
#include "request.h"
#include "response.h"

#define WIN32_LEAN_AND_MEAN
#include <stdio.h>
#include <stdlib.h>
#include <windows.h>
#include <winsock2.h>
#include <ws2tcpip.h>

#include <array>
#include <atomic>
#include <functional>
#include <iostream>
#include <map>
#include <memory>
#include <mutex>
#include <sstream>
#include <stdexcept>
#include <thread>
#include <vector>

// Need to link with Ws2_32.lib
#pragma comment (lib, "Ws2_32.lib")

//#define DBGPRINT

// constants to tune
const size_t RECV_BUF_LENGTH = 512;
const size_t MAX_SEND_BLOCK = 4096;
const unsigned short PORT = 80;

class TWinAPIException : public std::runtime_error {
public:
    TWinAPIException(const char* message, int errorCode)
        : std::runtime_error(message)
        , ErrorCode(errorCode)
    {
    }
    int GetErrorCode() const {
        return ErrorCode;
    }
private:
    int ErrorCode;
};

class TWSAException : public TWinAPIException {
public:
    TWSAException(const char* message, int errorCode)
        : TWinAPIException(message, errorCode)
    {
    }
};


class TWSAContext {
public:
    TWSAContext() {
        WSADATA wsaData;
        const int result = WSAStartup(MAKEWORD(2, 2), &wsaData);
        if (result != 0) {
            throw TWSAException("WSAStartup failed", WSAGetLastError());
        }
        if (LOBYTE(wsaData.wVersion) != 2 || HIBYTE(wsaData.wVersion) != 2) {
            WSACleanup();
            throw std::runtime_error("Could not find a usable version of Winsock.dll");
        }
        std::cerr << "WSA 2.2 Initialized" << std::endl;
    }

    ~TWSAContext() {
        if (WSACleanup() != 0) {
            std::cerr << "WSACleanup failed" << std::endl;
        }
    }
};


class TSocketWrapper {
public:
    TSocketWrapper()
        : Socket(INVALID_SOCKET)
    {
    }
    TSocketWrapper(SOCKET socket)
        : Socket(socket)
    {
    }
    TSocketWrapper(TSocketWrapper&& other)
        : Socket(INVALID_SOCKET)
    {
        std::swap(Socket, other.Socket);
    }

    SOCKET Get() const {
        return Socket;
    }
    ~TSocketWrapper() {
        if (Socket != INVALID_SOCKET) {
            if (closesocket(Socket) != 0) {
                std::cerr << "Closing socket failed: " << WSAGetLastError() << std::endl;
            }
        }
    }
    explicit operator bool() const {
        return (Socket != INVALID_SOCKET);
    }
private:
    TSocketWrapper(const TSocketWrapper& other) = delete;
    TSocketWrapper& operator=(const TSocketWrapper&) = delete;

protected:
    SOCKET Socket;
};


class TListeningSocketWrapper : public TSocketWrapper {
public:
    TListeningSocketWrapper(unsigned short port) {
        sockaddr_in serverAddr;

        // Overlapped I/O follows the model established in Windows and can be performed only on 
        // sockets created through the WSASocket function.
        Socket = WSASocket(AF_INET, SOCK_STREAM, IPPROTO_TCP, NULL, 0, WSA_FLAG_OVERLAPPED);

        if (Socket == INVALID_SOCKET) {
            throw TWSAException("Error while opening socket", WSAGetLastError());
        }

        // Cleanup and Init with 0 the serverAddr
        ZeroMemory(&serverAddr, sizeof(serverAddr));

        // Fill up the address structure
        serverAddr.sin_family = AF_INET;
        serverAddr.sin_addr.s_addr = INADDR_ANY; // WinSock will supply address
        serverAddr.sin_port = htons(port);

        // Assign local address and port number
        const int bindResult = bind(Socket, (struct sockaddr *) &serverAddr, sizeof(serverAddr));
        if (bindResult != 0) {
            throw TWSAException("Error while binding socket", WSAGetLastError());
        }

        // Make the socket a listening socket
        const int listenResult = listen(Socket, SOMAXCONN);
        if (listenResult != 0) {
            throw TWSAException("Error occurred while listening", WSAGetLastError());
        }
    }
};


class TWSAEventWrapper {
public:
    TWSAEventWrapper() {
        Event = WSACreateEvent();
        if (Event == WSA_INVALID_EVENT) {
            throw TWSAException("unable to create WSA event", WSAGetLastError());
        }
    }
    ~TWSAEventWrapper() {
        if (!WSACloseEvent(Event)) {
            std::cerr << "WSACloseEvent failed" << std::endl;
        }
    }
    const WSAEVENT& Get() const {
        return Event;
    }
private:
    TWSAEventWrapper(const TWSAEventWrapper& other) = delete;
    TWSAEventWrapper& operator=(const TWSAEventWrapper&) = delete;

private:
    WSAEVENT Event;
};


class TClient {
public:
    enum EOperation {
        No,
        RecvPending,
        SendPending,
    };

public:
    TClient(const TContentToServe& content, TSocketWrapper&& socket)
        : Content(content)
        , Socket(std::move(socket))
        , CurrentOp(EOperation::No)
        , RecvFlags(0)
        , TotalBytesTransferred(0)
    {
        ZeroMemory(&OverlappedStruct, sizeof(OverlappedStruct));

        RecvBuf.len = static_cast<ULONG>(RecvData.size());
        RecvBuf.buf = RecvData.data();
    }

    ~TClient() {
        assert(IsCompleted());
    }

    // is called from accept thread
    void InitialReceive() {
        DoRecv();
    }

    // is called from working thread
    void CompleteIOOperation(DWORD bytesTransferred) {
        TotalBytesTransferred += bytesTransferred;

#ifdef DBGPRINT
        std::cerr << "Completed IO (" << bytesTransferred << " bytes)" << std::endl;
#endif

        switch (CurrentOp)
        {
        case TClient::RecvPending:
            DoRecvDone(bytesTransferred);
            break;
        case TClient::SendPending:
            DoSendDone(bytesTransferred);
            break;
        default:
            throw std::runtime_error("end of IO operation received, but no op is pending");
        }
    }

    bool IsCompleted() const {
        return (CurrentOp == EOperation::No) || HasOverlappedIoCompleted(&OverlappedStruct);
    }
    size_t GetTotalBytesTrasferred() const {
        return TotalBytesTransferred;
    }
    EOperation GetOperation() const {
        return CurrentOp;
    }
private:
    void DoProcessRequest() {
        assert(!Request.IsComplete());

        Request.Read(&MemoryInput);
        if (!Request.IsComplete()) {
            DoRecv();
        } else {
            MakeResponse();
            DoSend();
        }
    }

    void DoRecv() {
        RecvFlags = 0;
        ZeroMemory(&OverlappedStruct, sizeof(OverlappedStruct));

        CurrentOp = EOperation::RecvPending;
        const int result = WSARecv(Socket.Get(), &RecvBuf, 1, NULL, &RecvFlags, &OverlappedStruct, NULL);
        const int error = WSAGetLastError();

        if (result == 0 || (result == SOCKET_ERROR && error == WSA_IO_PENDING)) {
#ifdef DBGPRINT
            std::cerr << "Pending Recv " << result << ' ' << error << std::endl;
#endif
        } else {
            CurrentOp = EOperation::No;
            throw TWSAException("WSARecv failed", error);
        }
    }

    void DoRecvDone(DWORD bytesReceived) {
#ifdef DBGPRINT
        std::cerr << "Recv Done: " << bytesReceived << '\n';
        std::cerr << "Data: '" << std::string(RecvData.begin(), RecvData.begin() + bytesReceived) << "'" << std::endl;
#endif
        // BytesReceived and RecvData is assumed to be valid
        MemoryInput = TMemoryInput(TDataRegion(RecvData.data(), bytesReceived));
        DoProcessRequest();
    }

    void MakeResponse() {
        assert(Request.IsComplete());
        if (Request.GetStatus() == THttpRequest::EStatus::OK) {
            const TDataRegion data = Content.GetResource(Request.GetURI());
            if (data) {
                Response.Init(data.GetData(), data.GetLength());
            } else {
                Response.Init(THttpResponse::ECode::NotFound);
            }
        } else {
            Response.Init(THttpResponse::ECode::BadRequest);
        }
    }

    void DoSend() {
        SendFlags = 0;
        ZeroMemory(&OverlappedStruct, sizeof(OverlappedStruct));

        Response.Prepare(MAX_SEND_BLOCK);

        assert(!Response.IsFullySent());
        WSABUF* bufs = Response.GetBuffers();
        DWORD bufCount = Response.GetBufferCount();

        CurrentOp = EOperation::SendPending;
        const int result = WSASend(Socket.Get(), bufs, bufCount, NULL, SendFlags, &OverlappedStruct, NULL);
        const int error = WSAGetLastError();

        if (result == 0 || (result == SOCKET_ERROR && error == WSA_IO_PENDING)) {
#ifdef DBGPRINT
            std::cerr << "Pending Send" << std::endl;
#endif
        } else {
            CurrentOp = EOperation::No;
            throw TWSAException("WSASend failed", error);
        }
    }

    void DoSendDone(DWORD bytesSent) {
        Response.Advance(bytesSent);
        if (Response.IsFullySent()) {
            // new request
            Request = THttpRequest();
            DoProcessRequest();
        } else {
            DoSend();
        }
    }
private:
    const TContentToServe& Content;
    TSocketWrapper Socket;
    OVERLAPPED OverlappedStruct;

    EOperation CurrentOp;

    WSABUF RecvBuf;
    DWORD RecvFlags;
    std::array<char, RECV_BUF_LENGTH> RecvData;
    TMemoryInput MemoryInput;
    THttpRequest Request;

    DWORD SendFlags;
    THttpResponse Response;

    size_t TotalBytesTransferred;
};


using TClientPtr = std::unique_ptr<TClient>;

class TServerContext {
public:
    TServerContext()
        : ClientCounter(0)
    {
        IOCompletionPort = CreateIoCompletionPort(INVALID_HANDLE_VALUE, NULL, 0, 1);
        if (IOCompletionPort == INVALID_HANDLE_VALUE) {
            throw TWinAPIException("CreateIoCompletionPort (IOCP creation failed)", GetLastError());
        }
    }

    ~TServerContext() {
        CloseHandle(IOCompletionPort);
    }

    void InitNewClient(TSocketWrapper&& socket) {
        // Associate the socket with IOCP
        ++ClientCounter;
        const size_t clientID = ClientCounter;

        const HANDLE hTemp = CreateIoCompletionPort((HANDLE)socket.Get(), IOCompletionPort, clientID, 0);
        if (hTemp == NULL) {
            throw TWinAPIException("CreateIoCompletionPort (association failed)", GetLastError());
        }
        assert(hTemp == IOCompletionPort);

        AddToMap(clientID, TClientPtr(new TClient(ContentToServe, std::move(socket))));
        const auto& client = GetClientByID(clientID);

        try {
            client->InitialReceive();
        } catch (const TWinAPIException& ex) {
            // assume that we have no pending IO
            std::cerr << "ERROR: initial Recv on client " << clientID << " failed: " << ex.what() << '\n';
            if (client->IsCompleted()) {
                RemoveFromMap(clientID);
            }
        }
    }

    void NotifyClientDeath(size_t clientID) {
        RemoveFromMap(clientID);
    }

    const TClientPtr& GetClientByID(size_t clientID) {
        std::lock_guard<std::mutex> lock(Mutex);
        const auto it = ActiveClients.find(clientID);
        if (it != ActiveClients.end()) {
            return it->second;
        } else {
            return NullPtr;
        }
    }

    void ShutDown() {
        ShutdownFlag = true;
    }
    bool NeedToShutDown() const {
        return ShutdownFlag;
    }
    const TContentToServe& GetContentToServe() const {
        return ContentToServe;
    }
    HANDLE GetIOCompletionPort() const {
        return IOCompletionPort;
    }

    size_t GetActiveClientCount() const {
        std::lock_guard<std::mutex> lock(Mutex);
        return ActiveClients.size();
    }
    void EnumClients() const {
        std::lock_guard<std::mutex> lock(Mutex);
        for (const auto& it : ActiveClients) {
            std::cerr << " * " << it.first << " - " << it.second->IsCompleted() << " : " << it.second->GetTotalBytesTrasferred() << " st " << (int)it.second->GetOperation() << '\n';
        }
    }
private:
    void AddToMap(size_t clientID, TClientPtr&& client) {
        std::lock_guard<std::mutex> lock(Mutex);
        ActiveClients[clientID] = std::move(client);
    }
    void RemoveFromMap(size_t clientID) {
        std::lock_guard<std::mutex> lock(Mutex);
        const auto it = ActiveClients.find(clientID);
        if (it != ActiveClients.end()) {
            ActiveClients.erase(it);
        }
    }

private:
    std::atomic<bool> ShutdownFlag;
    TContentToServe ContentToServe;

    size_t ClientCounter;

    mutable std::mutex Mutex;
    std::map<size_t, TClientPtr> ActiveClients;

    TClientPtr NullPtr;

    HANDLE IOCompletionPort;
};


void WorkerThread(TServerContext* ctx)
{
    while (!ctx->NeedToShutDown()) {
        DWORD bytesTransferred = 0;
        ULONG_PTR clientID = 0;
        OVERLAPPED* overlappedStruct = 0;

        const BOOL result = GetQueuedCompletionStatus(ctx->GetIOCompletionPort(), &bytesTransferred, &clientID, &overlappedStruct, INFINITE);

        assert(clientID != 0);

        if (!result || (bytesTransferred == 0)) {
            //Client connection gone, remove it.
            ctx->NotifyClientDeath(clientID);
            continue;
        }

        const TClientPtr& client = ctx->GetClientByID(clientID);
        if (client) {
            assert(client->IsCompleted());
            try {
                client->CompleteIOOperation(bytesTransferred);
            } catch (const TWinAPIException& ex) {
                std::cerr << "ERROR: client " << clientID << " failed: " << ex.what() << '\n';
                if (client->IsCompleted()) {
                    ctx->NotifyClientDeath(clientID);
                }
            }
        }
    }
}

void WorkerThreadWrapper(TServerContext* ctx)
{
    try {
        WorkerThread(ctx);
    } catch (const std::exception& ex) {
        std::cerr << "exception:\n" << ex.what() << std::endl;
        throw;
    }
}

class TStatPrinter {
public:
    TStatPrinter()
        : LastPrinted(0)
    {
    }
    bool Print(size_t numClients) {
        static const ULONGLONG DELAY = 1000;

        const ULONGLONG ts = GetTickCount64();
        if (LastPrinted + DELAY < ts) {
            std::cerr << "[active clients: " << numClients << "]\n";
            LastPrinted = ts;
            return true;
        }
        return false;
    }
private:
    ULONGLONG LastPrinted;
};

class TServer {
public:
    void Run() {
        std::thread th([this]{ WorkerThreadWrapper(&ServerContext); });
        AcceptingLoop();
    }

    void AcceptingLoop() {
        TListeningSocketWrapper listeningSocket(PORT);
        TWSAEventWrapper acceptEvent;
        const DWORD acceptTimeout = 100;

        if (WSAEventSelect(listeningSocket.Get(), acceptEvent.Get(), FD_ACCEPT) == SOCKET_ERROR) {
            throw TWSAException("WSAEventSelect failed", WSAGetLastError());
        }

        WSANETWORKEVENTS wsaEvents;
        TStatPrinter stat;

        while (!ServerContext.NeedToShutDown()) {
            if (stat.Print(ServerContext.GetActiveClientCount())) {
                //ServerContext.EnumClients();
            }

            const DWORD waitRes = WSAWaitForMultipleEvents(1, &acceptEvent.Get(), FALSE, acceptTimeout, FALSE);
            if (waitRes == WSA_WAIT_EVENT_0) {
                if (WSAEnumNetworkEvents(listeningSocket.Get(), acceptEvent.Get(), &wsaEvents) == SOCKET_ERROR) {
                    throw TWSAException("WSAEnumNetworkEvents failed", WSAGetLastError());
                }

                if ((wsaEvents.lNetworkEvents & FD_ACCEPT) && (0 == wsaEvents.iErrorCode[FD_ACCEPT_BIT])) {
                    AcceptConnection(listeningSocket);
                }

            } else if (waitRes == WSA_WAIT_FAILED) {
                throw TWSAException("WSAWaitForMultipleEvents failed", WSAGetLastError());
            }
        }
    }

    void AcceptConnection(const TListeningSocketWrapper& listeningSocket) {
        sockaddr_in clientAddr;
        ZeroMemory(&clientAddr, sizeof(clientAddr));
        int clientAddrSize = sizeof(clientAddr);

        TSocketWrapper socket = WSAAccept(listeningSocket.Get(), reinterpret_cast<sockaddr*>(&clientAddr), &clientAddrSize, NULL, NULL);
        if (!socket) {
            throw TWSAException("WSAAccept failed", WSAGetLastError());
        }
        ServerContext.InitNewClient(std::move(socket));
    }

private:
    TWSAContext WSAContext;
    TServerContext ServerContext;
};

int _tmain(int /*argc*/, _TCHAR* /*argv[]*/)
{
    try {
        TServer().Run();
    } catch (const TWSAException& ex) {
        std::cerr << "WSA error code " << ex.GetErrorCode() << ":\n" << ex.what() << std::endl;
    } catch (const TWinAPIException& ex) {
        std::cerr << "WinAPI error code " << ex.GetErrorCode() << ":\n" << ex.what() << std::endl;
    } catch (const std::exception& ex) {
        std::cerr << "exception:\n" << ex.what() << std::endl;
        getchar();
    }
	return 0;
}
