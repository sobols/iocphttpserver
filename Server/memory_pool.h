#pragma once

#include <memory>
#include <functional>
#include <vector>
#include <cassert>

class TMemoryPool {
public:
    TMemoryPool(size_t blockSize, size_t capacity);
    ~TMemoryPool();

    size_t GetAvailableBlocks() const;

    char* NewRaw();
    void DeleteRaw(char* block);

    typedef std::unique_ptr<char, std::function<void(char*)>> THolder;
    THolder Create();

private:
    TMemoryPool(const TMemoryPool& other) = delete;
    TMemoryPool& operator=(const TMemoryPool&) = delete;

private:
    char* Data;
    size_t BlockSize;
    size_t TotalFree;
    size_t FirstFree;
    size_t Undefined;
    std::vector<size_t> NextFree;
};

TMemoryPool::TMemoryPool(size_t blockSize, size_t capacity)
    : BlockSize(blockSize)
    , TotalFree(capacity)
    , FirstFree(0)
    , Undefined(capacity)
    , NextFree(capacity)
{
    if (capacity == 0) {
        return;
    }
    Data = static_cast<char*>(_aligned_malloc(BlockSize * capacity, BlockSize));
    if (Data == 0) {
        throw std::bad_alloc();
    }
    for (size_t i = 0; i < NextFree.size(); ++i) {
        NextFree[i] = i + 1;
    }
    assert(NextFree.back() == Undefined);
}

TMemoryPool::~TMemoryPool()
{
    _aligned_free(Data);
}

size_t TMemoryPool::GetAvailableBlocks() const
{
    return TotalFree;
}

char* TMemoryPool::NewRaw()
{
    if (FirstFree == Undefined) {
        throw std::runtime_error("no free blocks available");
    }
    const size_t index = FirstFree;
    FirstFree = NextFree[FirstFree];
    --TotalFree;
    return Data + BlockSize * index;
}

void TMemoryPool::DeleteRaw(char* block)
{
    const ptrdiff_t delta = block - Data;
    if (delta < 0 || delta % BlockSize != 0) {
        throw std::runtime_error("bad block pointer");
    }

    const size_t index = delta / BlockSize;
    if (index >= NextFree.size()) {
        throw std::runtime_error("bad block pointer");
    }

    NextFree[index] = FirstFree;
    FirstFree = index;
    ++TotalFree;
}

TMemoryPool::THolder TMemoryPool::Create()
{
    return THolder(NewRaw(), [this](char* block) { DeleteRaw(block); });
}
