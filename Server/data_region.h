#pragma once

#include <cstdlib>

class TDataRegion {
public:
    TDataRegion()
        : Data(nullptr)
        , Length(0)
    {
    }
    TDataRegion(const char* data, size_t length)
        : Data(data)
        , Length(length)
    {
    }
    explicit operator bool() const {
        return Data != nullptr;
    }
    const char* GetData() const {
        return Data;
    }
    size_t GetLength() const {
        return Length;
    }

private:
    const char* Data;
    size_t Length;
};
