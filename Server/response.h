#pragma once

#define WIN32_LEAN_AND_MEAN
#include <Winsock2.h>

#include <array>
#include <sstream>
#include <string>
#include <utility>


class THttpResponse {
public:
    enum class ECode {
        OK,
        NotFound,
        BadRequest,
    };
    THttpResponse()
        : Pos(0)
        , SendBufferCount(0)
    {
    }

    void Init(ECode code) {
        DoInit(code, nullptr, 0);
    }
    void Init(const char* data, size_t length) {
        DoInit(ECode::OK, data, length);
    }

    size_t GetTotalSize() const {
        size_t sum = 0;
        for (const WSABUF buf : AllBuffers) {
            sum += buf.len;
        }
        return sum;
    }

    void Advance(size_t sent) {
        Pos += sent;
        assert(Pos <= GetTotalSize());
    }

    bool IsFullySent() const {
        assert(Pos <= GetTotalSize());
        return Pos == GetTotalSize();
    }

    void Prepare(size_t maxBytesToSend) {
        SendBufferCount = 0;

        size_t acc = 0;
        for (const WSABUF buf : AllBuffers) {
            const size_t beg = std::max<size_t>(acc, Pos);
            const size_t end = std::min<size_t>(acc + buf.len, Pos + maxBytesToSend);
            if (beg < end) {
                SendBuffers[SendBufferCount].len = static_cast<ULONG>(end - beg);
                SendBuffers[SendBufferCount].buf = buf.buf + (beg - acc);
                ++SendBufferCount;
            }
            acc += buf.len;
        }
    }

    WSABUF* GetBuffers() const {
        return const_cast<WSABUF*>(SendBuffers.data());
    }
    DWORD GetBufferCount() const {
        return SendBufferCount;
    }

private:
    void DoInit(ECode code, const char* data = 0, size_t length = 0) {
        Pos = 0;
        SendBufferCount = 0;

        static const char endl[] = "\r\n";

        std::ostringstream oss;
        switch (code) {
        case ECode::OK:
            oss << "HTTP/1.1 200 OK" << endl;
            oss << "Connection: Keep-Alive" << endl;
            oss << "Content-Type: text/plain" << endl;
            SetBody(data, length);
            break;

        case ECode::NotFound:
            oss << "HTTP/1.1 404 Not Found" << endl;
            oss << "Connection: Keep-Alive" << endl;
            oss << "Content-Type: text/html" << endl;
            WriteCustomBody(404, "Not Found");
            SetBody(CustomBodyText.c_str(), CustomBodyText.length());
            break;

        case ECode::BadRequest:
            oss << "HTTP/1.1 400 Bad Request" << endl;
            oss << "Content-Type: text/html" << endl;
            WriteCustomBody(400, "Bad Request");
            SetBody(CustomBodyText.c_str(), CustomBodyText.length());
            break;
        }
        oss << "Content-Length: " << AllBuffers[Body].len << "\r\n";
        oss << "\r\n";

        HeaderText = std::move(oss.str());
        AllBuffers[Header].buf = &HeaderText[0];
        AllBuffers[Header].len = static_cast<ULONG>(HeaderText.length());
    }

    void WriteCustomBody(int code, const char* message) {
        std::ostringstream oss;
        oss << "<html>";
        oss << "<body>";
        oss << "<h1>" << code << " &mdash; " << message << "</h1>";
        oss << "<hr>";
        oss << "</body>";
        oss << "</html>";
        CustomBodyText = std::move(oss.str());
    }

    void SetBody(const char* data, size_t length) {
        AllBuffers[Body].buf = const_cast<char*>(data);
        AllBuffers[Body].len = static_cast<ULONG>(length);
    }
private:
    std::string HeaderText;
    std::string CustomBodyText;

    enum {
        Header = 0,
        Body = 1,
    };
    std::array<WSABUF, 2> AllBuffers;

    size_t Pos;

    DWORD SendBufferCount;
    std::array<WSABUF, 2> SendBuffers;
};
